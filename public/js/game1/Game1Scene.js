/**
 * Game1Scene Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class Game1Scene {
    
    /**
     * clears the document.body 
     * calls the DataHelper to get 3 words or whatever, and the DataHelper marks them as used for this session then
     * (how should it know the level name? "animals1" or "music2", .. ? ) (after I create the menu scene, later)
     * creates all the elements in the scene within their respected places and sets them up 
     * (if they have animations it should start on their creation time?) not now, we didnt agree on animations
     * 
     * list of created objects (arranged top to bottom on screen DOM arrangement):
     * - background, random places for stars? 
     * - back button
     * - title
     * - score
     * - track (give it the 3 words)
     * - 3 draggables (give each one a word)
     * 
     * then sets a global variable activeScene to 'this' variable, for the events to be able to access it
     * after that the control is handed to the events taken by user, score is time functioning and editing its model only
     */

    constructor(){

      //public
      this.levelWords = [];
      this.usedwords = [];
      this.starsbackground = [];
      this.draggablecards = [];
      this.backbutton = null;
      this.title = null;
      this.score = null;
      this.track = null;
      this.canvas = null;
      this.revisemode = false;
      this.revisecounter = 0;
      this.endrevisemodecounter = 0;
      window.activeScene = this;

      // ================= START SCENE CREATION
      // get data
      //document.body.innerHTML = ''; //body mst contain div content element
      this.levelWords = DataHelper.shuffle(DataHelper.getdata("Animals"));

      // create background
      document.body.style.backgroundColor = "#293aa8";
      // add stars [x,y,width]
      // TODO : create div to hold them all // change them into paperjs drawings and animate them on screen scrolling
      let starpos = [[0.04, 100,30], [0.29, 70,50],[0.53, 85,50],[0.68, 100,26],[0.09, 500,80],[0.78, 520,20],[0.87, 600,30],
                     [0.09, 0,20], [0.36, 90,20],[0.63, 60,50],[0.04, 500,30],[0.48, 450,20],[0.73, 670,30],[0.87, 700,30]]
      for (let i = 0; i < 14; i++){
        let starelem = document.createElement("img");
        starelem.src = 'img/yellowstar.png';
        starelem.style = "position: fixed; left: " + 
                          starpos[i][0]*window.standardresolution.width + 
                          "px; top: " + starpos[i][1] + "px; width:" + starpos[i][2] + "px; z-index: -1;"
        document.getElementById("content").appendChild(starelem);
        this.starsbackground.push(starelem);
        if (i >= 7) starelem.src = 'img/whitestar.png';
      }

      // create backbutton
      this.backbutton = new Back();
      DomHelper.setpos(this.backbutton.DOM, 30, 30);
      DomHelper.add(this.backbutton.DOM);

      // create title
      this.title = new Title("Animals", 1);
      // DomHelper.setpos(this.title.DOM, 620, 25);
      // DomHelper.add(this.title.DOM);
      // this.title.DOM.style.color = 'white';

      // create track
      // track is not DOM elements, they are drawn on canvas using paperJS so they are treated diffferently
      this.usedwords = this.usedwords.concat(this.levelWords.splice(0, 3));
      this.track = new Track(this.title.string, this.usedwords, {x: 0, y: 550});
      this.track.animateEntrance();

      // create draggable cards
      // draggable cards are not DOM elements, they are drawn on canvas using paperJS so they are treated diffferently
      let newwords = DataHelper.shuffle(this.usedwords)
      for (let i = 0; i < 3; i++){
        let y = 600; if (i == 1) y = 550;
        let card = new DraggableCard(newwords[i], 90, {x: 450 + i * 230, y: y});
        card.animateEntrance();
        this.draggablecards.push(card);
      }

      // create score
      this.score = new Score();
      this.score.animateEntrance();
    }

    /**
     * goes through all the created objects listed above and updates the DOM model accordingly
     * no need for a loop because it's not dynamic
     * **remember, animations anywhere must have an active flag to prevent this function from playing them while playing
     * **responsible for giving the 3 new word/s to track and creating 3/1 cards on all slots being unavailable 
     * // will the cards be created here or will they need another function to get called by the scrollscreen animation callback
     * // to be able to still have reference on the old cards for the animation before deleting them
     */
    updatescene(flag){
      // first, check if all slots are done, to call the flip screen
      let flipscreen = true;
      for (let i = 0; i < this.draggablecards.length; i++)
        flipscreen = flipscreen && !this.draggablecards[i].available;

      if (flipscreen){
        if ((this.usedwords.length % 9 == 0 || this.revisemode) && !flag){
          if (this.endrevisemodecounter == 9) { // end revise mode
            this.revisemode = false;
            this.endrevisemodecounter = 0;
            if (this.levelWords.length == 0){
              // show game-end popup
              console.log("GAME END!")
              this.endgame1scene();
              return;
            }
            this.updatescene(true);
            return;
          }
          console.log("send revision")
          this.revisemode = true;
          this.endrevisemodecounter++;
          let newwords = [this.usedwords[this.revisecounter++]];
          newwords.push(this.usedwords[Math.floor(Math.random()*(this.usedwords.length-0.01))])
          newwords.push(this.usedwords[Math.floor(Math.random()*(this.usedwords.length-0.01))]) // BUG can be same word

          // send revision
          this.track.scrollscreen(newwords, false);
          window.activeScene.resetcards(newwords);

        } else if (this.levelWords.length >= 3){
          console.log("normal case")
          let newwords = this.levelWords.splice(0, 3)
          this.usedwords = this.usedwords.concat(newwords);
  
          this.track.scrollscreen(newwords, false);
          window.activeScene.resetcards(newwords);
        } else {
          console.log("finished words!") // BUG - wont revise except first 9-ish words
          let newwords = this.levelWords;
          this.levelWords = [];
          while (newwords.length < 3) newwords.push(this.usedwords[Math.floor(Math.random()*(this.usedwords.length-0.01))])
          this.usedwords = this.usedwords.concat(newwords);
         
          this.track.scrollscreen(newwords, false);
          window.activeScene.resetcards(newwords);
        }

      }

    }

    resetcards(newwords){
        this.draggablecards = [];
        let revisedeltapos = 0;
        newwords = DataHelper.shuffle(newwords)
        if (this.revisemode) revisedeltapos = 230;
        for (let i = 0; i < newwords.length; i++){
          let y = 600; if (i == 1) y = 550;
          let card = new DraggableCard(newwords[i], 90, {x: 450 + i * 230 + revisedeltapos, y: y});
          card.animateEntrance();
          this.draggablecards.push(card);
          if (this.revisemode) return; // only one card if revise, and it's the first in the array
        }  
    }

    endgame1scene(){
      document.getElementById("myModal").style.display = "block";
      clearInterval(this.score.timeticking);
    }

    /**
     * goes through all the created objects listed above and calls their delete function then redirects to the menu 
     * !called on clickin back, or winning the game
     */
    deletescene(){

    }
}