/**
 * Title Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class Title {
    
    /**
     * fixed position for now, but later can add functions to set position with css
     * based on constructor arguments
     */
    constructor(string, headernumber) {
        this.string = string;
        this.headernumber = headernumber;
        this.DOM = this.generateDOM();
    }


    /**
     * Generates the DOM object to be added to the html file, dont draw, only generate
     */
    generateDOM(){
        let el = document.createElement("h"+this.headernumber);
        let t = document.createTextNode(this.string);
        el.appendChild(t);
        return el;
    }
  }
  