/**
 * DomHelper Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class DomHelper {

    /**
     * adds (creates, not edit) the desired element as a child to the document body
     * @param el: the DOM object that is going to be added
     */
    static add(el){
        document.getElementById("content").appendChild(el);
    }

    /**
     * sets position of DOM element
     */
    static setpos(DOM, x, y){
        DOM.style = "position: fixed; left: " + x + "px; top: " + y + "px;";
    }

    /* more functions to come ..  */
}  
