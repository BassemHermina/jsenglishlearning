/**
 * DataHelper Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class DataHelper {

    /**
     * may be the instructior should have all the info to open the socket or whatever credentials
     * needed to get the data from firebase, but for now, no need
     */
    constructor(){
      
    }

    /**
     * this function should get the array of words from firebase using the parameter level name, later will 
     * also contain game name
     * @param dataid: string of level id to get the database of this level 
     */
    static getdata(dataid){
        
        //dummy for offline database
        return window[dataid];
    }

    static shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
        return array;
      }
    
  
}
    