/**
 * Track Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class Track extends PaperItem{

    constructor(title, words, position) {
        super();

        // initializers
        this.titlestring = title;
        this.currentwordsarr = words;
        this.position = position;
        this.pointsdata = DataHelper.getdata("track0");
        this.screenIndex = 0;
        this.firstscreenflag = true;
        this.slots = [];
        this.nextslots = [];
        
        // create white track
        this.whitetrack = new paper.Path();
        this.whitetrack.fillColor = 'white';
        for (let i = 0; i < this.pointsdata.length/2; i++)
            this.whitetrack.add(new paper.Point(this.pointsdata[i][0], this.pointsdata[i][1]));
        // should these magic numbers go into the offline/db or not worth it?
        this.whitetrack.smooth({ type: 'continuous', from: 0, to: 2 });
        this.whitetrack.smooth({ type: 'continuous', from: 3, to: 6 });
        for (let i = this.pointsdata.length/2; i < this.pointsdata.length; i++){
            if (this.pointsdata[i][0]) 
                this.whitetrack.segments[i-this.pointsdata.length/2].handleIn.setAngle(this.pointsdata[i][0]);
            if (this.pointsdata[i][1])
                this.whitetrack.segments[i-this.pointsdata.length/2].handleOut.setAngle(this.pointsdata[i][1]);
        }
        
        // create colored slots
        this.slots[0] = new Slot(" ", Color.getrandc(), 0, this.screenIndex, false)
        this.slots[1] = new Slot(this.currentwordsarr[0], Color.getrandc(), 1, this.screenIndex, true)
        this.slots[2] = new Slot(this.currentwordsarr[1], Color.getrandc(), 2, this.screenIndex, true)
        this.slots[3] = new Slot(this.currentwordsarr[2], Color.getrandc(), 3, this.screenIndex, true)
        this.slots[4] = new Slot(" ", Color.getrandc(), 4, this.screenIndex, false)
        
        /////// first screen special case
        this.slots[1].label.translate(40, 5)
        // create coverup
        this.firstscreencover = new paper.Path();
        this.firstscreencover.fillColor = document.body.style.backgroundColor;
        this.firstscreencover.add(new paper.Point(-5,190))
        this.firstscreencover.add(new paper.Point(120,190))
        this.firstscreencover.add(new paper.Point(130,415))
        this.firstscreencover.add(new paper.Point(-5,430))
        this.lastscreencover = new paper.Path();
        this.lastscreencover.fillColor = document.body.style.backgroundColor;
        this.lastscreencover.add(new paper.Point(1440+1390,190))
        this.lastscreencover.add(new paper.Point(1440+1445,190))
        this.lastscreencover.add(new paper.Point(1440+1445,430))
        this.lastscreencover.add(new paper.Point(1440+1355,430))
        // TODO set z-index of cover to be ontop of all slots
        // create white circle
        this.circle = new paper.Path.Circle({
            center: new paper.Point(190,280),
            radius: 150
        });
        this.circle.fillColor = 'white';
        this.circle.segments[1].handleIn.setAngle(175);
        this.circle.segments[1].handleOut.setAngle(3);
        this.circle.segments[2].point.setY(this.circle.segments[2].point.getY() + 10);
        this.circle.segments[2].handleOut.setY(90);
        this.label = new paper.PointText(this.circle.position.add(0,15));
        this.label.rotate(8);
        this.label.justification = 'center';
        this.label.fillColor = 'black';
        this.label.content = this.titlestring;
        this.label.fontSize = '62px'
        this.label.fontFamily = 'boardgamers'

        this.initializeresponsivescaling();
    }

    /**
     * 
     * @param {*} threewords 
     * @param {*} lastscreenflag 
     * this function should be called by Game1Scene update scene function when
     * it finds out that all 3 playable slots in the track are unavailable
     */
    scrollscreen(threewords, lastscreenflag){
        let globalscale = window.innerWidth / window.standardresolution.width;
        this.pointsdata = DataHelper.getdata("track" + (this.screenIndex+1)%2);
        PaperItem.list = [];

        // extend white track
        for (let i = 1; i < this.pointsdata.length/2 - 1; i++)
            this.whitetrack.insert(i+2, new paper.Point((1440+this.pointsdata[i][0])*globalscale,this.pointsdata[i][1]*globalscale))
        this.whitetrack.smooth({ type: 'continuous', from: 2, to: 4 });
        this.whitetrack.smooth({ type: 'continuous', from: 5, to: 7 });
        for (let i = this.pointsdata.length/2; i < this.pointsdata.length; i++){
            if (this.pointsdata[i][0]!=null) 
                this.whitetrack.segments[i-this.pointsdata.length/2 + 2].handleIn.setAngle(this.pointsdata[i][0]);
            if (this.pointsdata[i][1]!=null)
                this.whitetrack.segments[i-this.pointsdata.length/2 + 2].handleOut.setAngle(this.pointsdata[i][1]);
        }
        PaperItem.list.push(this);  

        // create new slots outside of screen
        let handler = 0;
        if (this.screenIndex == 1) handler = 1440; 
        this.nextslots[1] = new Slot(threewords[0], Color.getrandc(), 1, (this.screenIndex+1)%2, true, handler);
        this.nextslots[2] = new Slot(threewords[1], Color.getrandc(), 2, (this.screenIndex+1)%2, true, handler);
        this.nextslots[3] = new Slot(threewords[2], Color.getrandc(), 3, (this.screenIndex+1)%2, true, handler);
        this.nextslots[4] = new Slot(" ", Color.getrandc(), 4, (this.screenIndex+1)%2, false, handler);
        // TODO z-index last screen cover?

        // translate eveything left
        let group = new paper.Group([
            this.whitetrack, 
            this.slots[0].path, this.slots[0].label,
            this.slots[1].path, this.slots[1].label,
            this.slots[2].path, this.slots[2].label,
            this.slots[3].path, this.slots[3].label,
            this.slots[4].path, this.slots[4].label,
            this.nextslots[1].path, this.nextslots[1].label,
            this.nextslots[2].path, this.nextslots[2].label,
            this.nextslots[3].path, this.nextslots[3].label,
            this.nextslots[4].path, this.nextslots[4].label,
            this.firstscreencover,
            window.activeScene.draggablecards[0].circle,
            window.activeScene.draggablecards[0].label,
        ]);
        if (window.activeScene.draggablecards[1]) group.addChild(window.activeScene.draggablecards[1].circle)
        if (window.activeScene.draggablecards[1]) group.addChild(window.activeScene.draggablecards[1].label)
        if (window.activeScene.draggablecards[2]) group.addChild(window.activeScene.draggablecards[2].circle)
        if (window.activeScene.draggablecards[2]) group.addChild(window.activeScene.draggablecards[2].label)
        if (lastscreenflag) group.addChild(this.lastscreencover)
        if (this.firstscreenflag) group.addChild(this.circle) 
        if (this.firstscreenflag) group.addChild(this.label) 


        this.moving = true;
        anime({
            targets: group.position,  
            x: -1440 * globalscale + group.position.x, 
            easing:  'easeOutElastic(1, 0.8)',
            complete:function(){ 
                this.whitetrack.removeSegments(0,2); 
                this.whitetrack.removeSegments(6,9);
                this.slots[0].delete();
                this.slots[1].delete();
                this.slots[2].delete();
                this.slots[3].delete();
                this.circle.translate(0, -2000); this.label.translate(0, -2000);  // delete circle and label
        
                this.slots[0] = this.slots[4]
                this.slots[1] = this.nextslots[1]
                this.slots[2] = this.nextslots[2]
                this.slots[3] = this.nextslots[3]
                this.slots[4] = this.nextslots[4]
                
                this.moving = false;
                this.resetresponsivescaling();                    
            }.bind(this)
        });

        this.firstscreenflag = false;
        this.screenIndex = (this.screenIndex+1)%2

    }

    markunavailableslot(index){
        let slotloop = 0;
        while (index != 0){
            if (slots[slotloop].available) index--;
            if (index == 0) slots[slotloop].available = false
        }   
    }

    /**
     * Plays the entrance animation of the Class, if any
     */
    animateEntrance(){
        // this.label.scaling.x = 0.5; 
        // this.label.scaling.y = 0.5; 
        // anime({
        //     targets: this.label.scaling,
        //     x: 0.8,
        //     y: 0.8,
        //     easing: 'easeOutElastic(1, 0.8)',
        // });

        this.moving = true;
        anime({
            targets: this.circle,
            rotation: 20,
            easing: 'easeOutElastic(1, 0.8)',
            complete:function(){                 
                this.moving = false;
            }.bind(this)
        });
    }

    /**
     * calls the animate exit function to get it out of screen and give it a timeout function that
     * deletes the DOM from the html page 
     */
    delete(){
    }   

    /**
     * Plays the exit animation of the Class, if any, by setting the CSS to new values and setting the animation 
     * curve to animate using the window responsive property, or using the animation library, still haven't decided
     * !called by the destructor of this class? or whoever want to destroy the object (delete it DOM)
     */
    animateExit(){
    }
  }
  