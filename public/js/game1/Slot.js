/**
 * DraggableCard Class
 * @author BassemHermina
 * Date: 6 Aug 2020
 */

 class Slot extends PaperItem{

    /**
     * 
     * @param string: label to be written in slot 
     * @param color: fillColor of the path
     * @param relativeindex: index of the slot on screen from left to right, first scren special case starts from 1 
     * @param screenindex: 0 or 1, the game has two shapes for the Track path with two slot point details
     */
    constructor(string, color, relativeindex, screenindex, available, handler = 0){
        super();

        //initializers
        this.labelstring = string;
        this.color = color;
        this.relativeindex = relativeindex;
        this.screenindex = screenindex;
        this.pointsdata = DataHelper.getdata("slot"+screenindex+relativeindex);
        this.available = available;

        //create slot
        this.path = new paper.Path();
        this.path.fillColor = this.color;
        for (let i = 0; i < 4; i++)
            this.path.add(new paper.Point(this.pointsdata[i][0]+ handler, this.pointsdata[i][1]));  
        for (let i = 4; i < 8; i++){
            if (this.pointsdata[i][0].length){
                this.path.segments[i-4].handleIn.setX(this.pointsdata[i][0][0]);
                this.path.segments[i-4].handleIn.setY(this.pointsdata[i][0][1]);
            }
            if (this.pointsdata[i][1].length){
                this.path.segments[i-4].handleOut.setX(this.pointsdata[i][1][0]);
                this.path.segments[i-4].handleOut.setY(this.pointsdata[i][1][1]);
            }
        }

        this.label = new paper.PointText(this.path.position);
        this.label.justification = 'center';
        this.label.fillColor = 'white';
        this.label.content = this.labelstring;
        this.label.fontSize = '48px'
        this.label.fontFamily = 'boardgamers'   
        this.label.rotate(this.pointsdata[8]);

        this.initializeresponsivescaling()
    }

    /**
     * called when slot is outside of the screen by the Track (he is its parent)
     */
    delete(){
        this.path.remove();
        this.label.remove();
        delete this;
    }

}