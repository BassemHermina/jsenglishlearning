/**
 * Back Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class Back {
    
    /**
     * fixed position for now, but later can add functions to set position with css
     * based on constructor arguments
     */
    constructor() {
        this.DOM = this.generateDOM();
    }


    /**
     * Generates the DOM object to be added to the html file, dont draw, only generate
     */
    generateDOM(){
        let el = document.createElement("div");
        let img = document.createElement("img");
        img.setAttribute("src", "img/back.png");
        img.style = "position: relative; left: -15px;"
        
        let text = document.createElement("h1");
        text.style = "color: white; margin: 0; padding: 0; position: relative; left: 20px;"
        let t = document.createTextNode("Back");
        text.appendChild(t);

        el.appendChild(text);
        el.appendChild(img);
        return el;
    }
  }
  