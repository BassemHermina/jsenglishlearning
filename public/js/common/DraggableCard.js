/**
 * DraggableCard Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class DraggableCard extends PaperItem{

    /**
     * @param string: used the word to search the image data base for the icon/image/png/jpg/.. 
     * @param radius
     * @param position: {x,y}
     * @param 
     */
    constructor(string, radius, position) {
        super();

        // initializers
        this.string = string;
        this.radius = radius;
        this.position = position;
        this.maxdistancetoslotcenter = this.radius*0.8;
        this.shadowcircle = null;
        this.available = true;
        this.blockevents = false;

        // create 
        this.circle = new paper.Path.Circle({
            center: new paper.Point(this.position.x,this.position.y),
            radius: this.radius
        });
        this.circle.fillColor = 'white';
        
        // get image 
        this.label = new paper.Raster(string);
        if (string == "sheep" || string == "horse") this.label.scale(0.8);   
        this.label.position = this.position;
        this.label.scale(0.65);

        // mouse events data
        this.mousehit = false;
        this.distancetocenter = 0;

        //setting event handlers
        this.onmousedown = this.onmousedown.bind(this); //to be able to detach the handler by having reference to the handler
        paper.view.attach("mousedown", this.onmousedown);
        this.onmouseup = this.onmouseup.bind(this); 
        paper.view.attach("mouseup", this.onmouseup);
        this.onmousedrag = this.onmousedrag.bind(this); 
        paper.view.attach("mousedrag", this.onmousedrag);

        this.initializeresponsivescaling();
    }

    onmousedown(event){
        if (this.circle.contains(event.point) && !this.blockevents) {
            this.mousehit = true;
            this.distancetocenter = event.point.subtract(this.circle.position);
        }
    }

    onmouseup(event){
        if (this.circle.contains(event.point) && !this.blockevents) {
            this.mousehit = false;
            for (let i = 0; i < window.activeScene.track.slots.length; i++)
                if (window.activeScene.track.slots[i].path.contains(event.point) 
                    && window.activeScene.track.slots[i].available 
                    && this.string == window.activeScene.track.slots[i].labelstring){
                        paper.view.detach("mousedown", this.onmousedown);
                        paper.view.detach("mouseup", this.onmouseup);
                        paper.view.detach("mousedrag", this.onmousedrag);
                        activeScene.score.correctcardplaced();
                        
                        this.moving = true;
                        anime({
                            targets: [this.circle.position, this.label.position],
                            x: window.activeScene.track.slots[i].label.position.x, //label pos better than slot pos
                            y: window.activeScene.track.slots[i].label.position.y, //label pos better than slot pos
                            easing: 'spring(1, 100, 16, 10)',
                            complete: function(){
                                window.activeScene.track.slots[i].available = false;
                                this.available = false;
                                this.moving = false;
                                window.activeScene.updatescene();     
                            }.bind(this)
                        });

                        // initiate reward animation
                        let scale = window.innerWidth / window.standardresolution.width;
                        for (let j = 0; j < 5; j++){
                            let star = new Star(window.activeScene.track.slots[i].path.position, scale);
                            let randpos = { 
                                x: (Math.random() * 60 + this.radius) * Math.sign(Math.random()*2-1) * scale, 
                                y: (Math.random() * 60 + this.radius) * Math.sign(Math.random()*2-1) * scale 
                            }
                            anime({
                                targets: [star.path.position, star.path],
                                x: star.path.position.x + randpos.x,
                                y: star.path.position.y + randpos.y, 
                                scaling: 0.92,
                                rotation: 7,
                                easing: 'spring(1, 100, 16, 10)',
                                complete: function(){
                                    this.path.remove();
                                }.bind(star)
                            });
                        }

                        
                        return;
                } 

                // not-placed-into-a-slot case
                this.blockevents = true;
                this.moving = true;
                anime({
                    targets: [this.circle.position, this.label.position],
                    x: this.position.x,
                    y: this.position.y, 
                    easing: 'spring(1, 100, 16, 10)',
                    complete: function(){
                        this.moving = false;
                        this.blockevents = false;
                    }.bind(this)
                });
                
        }
    }

    onmousedrag(event){
        if (this.mousehit && !this.blockevents) {
            this.circle.position = event.point.subtract(this.distancetocenter);
            this.label.position = event.point.subtract(this.distancetocenter);
        }
    }

    /**
     * Plays the entrance animation of the Class, if any, called by constructor
     */
    animateEntrance(){
        this.circle.translate(0, 500);
        this.label.translate(0, 500);
        this.blockevents = true;
        let randelay = Math.random()*500
        anime({
            targets: [this.circle.position, this.label.position],
            x: this.position.x,
            y: this.position.y, 
            easing: 'easeOutElastic(1, 0.8)',
            delay: randelay,
            complete: function(){
                this.blockevents = false; // heerereeeee was doing this
            }.bind(this)
        });
    }

    /**
     * calls the animate exit function to get it out of screen and give it a timeout function that
     * deletes the DOM from the html page 
     */
    delete(){
        this.circle.remove();
        this.label.remove();
        delete this;
    }   
  }
  