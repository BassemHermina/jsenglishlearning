/**
 * PaperItem Class
 * @author BassemHermina
 * Date: 6 Aug 2020
 */

 class PaperItem{

    constructor(){
        this.blockevents = true;
        this.moving = false;
        PaperItem.list.push(this);
    }

    /**
     * called by any class constructor which draws a paper object/s to screen, to handle it's auto resizing/scaling
     */
    initializeresponsivescaling(){
        for (var prop in this) 
            if (Object.prototype.hasOwnProperty.call(this, prop)) 
                if (this[prop] instanceof paper.Path||
                    this[prop] instanceof paper.PointText||
                    this[prop] instanceof paper.Path.Circle||
                    this[prop] instanceof paper.Raster) {  
                        this[prop].initialWidth = this[prop].bounds.width; // needed for the autoscaling function/event
                        //initial scale to fit to scaled window
                        this[prop].scale(window.innerWidth / window.standardresolution.width, new paper.Point(0,0));
                        this.position = this[prop].position;
                        paper.view.attach("resize", function(prop){
                            // first, check if any paperItem is moving, if so, return and raise a resize event for later
                            for (let i = 0; i < PaperItem.list.length; i++){
                                if (PaperItem.list[i].moving){
                                    // BUG, still exists some glitches on resizing, maybe I should reset scene anyway?
                                    setTimeout(function(){ 
                                        paper.view.emit('resize', { size: paper.view._viewSize, delta: paper.view._viewSize });
                                    }, 2000);                                    
                                    return;
                                }
                            }

                            let newabsolutescale = window.innerWidth / window.standardresolution.width;
                            let newdwidth = newabsolutescale * this[prop].initialWidth; 
                            let deltascale = newdwidth / this[prop].bounds.width;
                            this[prop].scale(deltascale, new paper.Point(0,0));
                            this.position = this[prop].position;
                        }.bind(this,prop));
                }
    }

    /**
     * called by any class containing paper object/s after editing the path/width of any paper object
     */
    resetresponsivescaling(){
        for (var prop in this) 
            if (Object.prototype.hasOwnProperty.call(this, prop)) 
                if (this[prop] instanceof paper.Path||
                    this[prop] instanceof paper.PointText||
                    this[prop] instanceof paper.Path.Circle) {  
                        this[prop].initialWidth = this[prop].bounds.width * window.standardresolution.width / window.innerWidth;
                }
    }

    onresize(prop){

    }
 }

 PaperItem.list = [];







