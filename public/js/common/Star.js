/**
 * Star Class
 * @author BassemHermina
 * Date: 8 Aug 2020
 */

class Star extends PaperItem{

    constructor(position,scale) {
        super();

        this.position = position;
        this.initialwidth = 500;
        this.initialheight = 500;

        var center = new paper.Point(position);
        var points = 5;
        var radius1 = 23;
        var radius2 = 40;
        this.path = new paper.Path.Star(center, points, radius1, radius2);
        this.path.scale(scale);
        this.path.fillColor = 'yellow';

        console.log("created a star")
    }

}