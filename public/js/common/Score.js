/**
 * Score Class
 * @author BassemHermina
 * Date: 2 Aug 2020
 */

class Score extends PaperItem{
    
    /**
     * 
     */
    constructor() {
        super();

        this.score = 0;
        this.ticker = 0;
        this.multiplicative = 1;

        // create arc for clock and search how to control it
        // put them into a group for easier moving and handling
        // create white circle

        this.group = new paper.Group([]);

        
        {
            this.scorecircle = new paper.Path.Circle({
                center: new paper.Point(190,280),
                radius: 150
            });
            this.scorecircle.fillColor = 'hotpink';
            this.scorecircle.segments[1].handleIn.setAngle(175);
            this.scorecircle.segments[1].handleOut.setAngle(3);
            this.scorecircle.segments[2].point.setY(this.scorecircle.segments[2].point.getY() + 10);
            this.scorecircle.segments[2].handleOut.setY(90);
            this.scorecircle.position = {x: 1256, y: 115}
            this.scorecircle.scale(0.48)
            this.scorecircle.rotate(90)
            this.scorecircle.strokeColor = 'yellow';
            this.scorecircle.strokeWidth = 8;
            this.group.addChild(this.scorecircle);
        }
        
        {
            this.titlescorenumber = new paper.PointText(this.scorecircle.position.add(0,5));
        this.titlescorenumber.justification = 'center';
        this.titlescorenumber.fillColor = 'white';
        this.titlescorenumber.content = '0';
        this.titlescorenumber.fontSize = '50px'
        this.titlescorenumber.fontFamily = 'boardgamers'
        this.group.addChild(this.titlescorenumber);
        }
        
        {
            this.titlescore = new paper.PointText(this.scorecircle.position.add(0,32));
        this.titlescore.justification = 'center';
        this.titlescore.fillColor = 'white';
        this.titlescore.content = 'score';
        this.titlescore.fontSize = '16px'
        this.titlescore.fontFamily = 'boardgamers'
        this.group.addChild(this.titlescore);
        }

        {
            this.timecirclebg = new paper.Path.Circle({
            center: new paper.Point(190,280),
            radius: 150
        });
        this.timecirclebg.fillColor = 'hotpink';
        this.timecirclebg.segments[1].handleIn.setAngle(175);
        this.timecirclebg.segments[1].handleOut.setAngle(3);
        this.timecirclebg.segments[2].point.setY(this.timecirclebg.segments[2].point.getY() + 10);
        this.timecirclebg.segments[2].handleOut.setY(90);
        this.timecirclebg.position = {x: 1350, y: 158}
        this.timecirclebg.scale(0.32, 0.3)
        this.timecirclebg.strokeColor = 'black';
        this.timecirclebg.strokeWidth = 8;
        this.group.addChild(this.timecirclebg);
        }

        {
            this.timecircleframe = new paper.Path.Circle({
            center: new paper.Point(190,280),
            radius: 150
        });
        this.timecircleframe.fillColor = null;
        this.timecircleframe.segments[1].handleIn.setAngle(175);
        this.timecircleframe.segments[1].handleOut.setAngle(3);
        this.timecircleframe.segments[2].point.setY(this.timecircleframe.segments[2].point.getY() + 10);
        this.timecircleframe.segments[2].handleOut.setY(90);
        this.timecircleframe.position = {x: 1350, y: 152}
        this.timecircleframe.scale(0.32, 0.3)
        this.timecircleframe.strokeColor = 'white';
        this.timecircleframe.strokeWidth = 8;
        this.group.addChild(this.timecircleframe);
        }

        {
            this.timecircleinstroke = new paper.Path.Circle({
            center: new paper.Point(190,280),
            radius: 150
        });
        this.timecircleinstroke.fillColor = null;
        this.timecircleinstroke.segments[1].handleIn.setAngle(175);
        this.timecircleinstroke.segments[1].handleOut.setAngle(3);
        this.timecircleinstroke.segments[2].point.setY(this.timecircleinstroke.segments[2].point.getY() + 10);
        this.timecircleinstroke.segments[2].handleOut.setY(90);
        this.timecircleinstroke.position = {x: 1350, y: 152}
        this.timecircleinstroke.scale(0.15, 0.14)
        this.timecircleinstroke.strokeColor = 'orange';
        this.timecircleinstroke.strokeWidth = 6;
        this.group.addChild(this.timecircleinstroke);
        }

        {
            this.timecirclecenter = new paper.Path.Circle({
            center: new paper.Point(190,280),
            radius: 150
        });
        this.timecirclecenter.fillColor = null;
        this.timecirclecenter.segments[1].handleIn.setAngle(175);
        this.timecirclecenter.segments[1].handleOut.setAngle(3);
        this.timecirclecenter.segments[2].point.setY(this.timecirclecenter.segments[2].point.getY() + 10);
        this.timecirclecenter.segments[2].handleOut.setY(90);
        this.timecirclecenter.position = {x: 1350, y: 152}
        this.timecirclecenter.scale(0.04)
        this.timecirclecenter.strokeColor = 'yellow';
        this.timecirclecenter.strokeWidth = 6;
        this.timecirclecenter.setShadowOffset(2);
        this.timecirclecenter.setShadowColor('black');
        this.group.addChild(this.timecirclecenter);
        }

        {
            this.titlescoremultiply = new paper.PointText(this.timecircleframe.position.add(10,-60));
        this.titlescoremultiply.justification = 'center';
        this.titlescoremultiply.fillColor = 'white';
        this.titlescoremultiply.content = ' ';
        this.titlescoremultiply.fontSize = '30px'
        this.titlescoremultiply.fontFamily = 'boardgamers'
        this.group.addChild(this.titlescoremultiply);
        }

        let arcValues = this.getCreateArcInfo( 0, { x: 1350, y: 152 }, 40)
        this.timearc = new paper.Path.Arc(arcValues)
        this.timearc.name = 'timearc';
        this.group.addChild(this.timearc);

        this.initializeresponsivescaling();
    }


    /**
     * Plays the entrance animation of the Class, if any, called by constructor
     */
    animateEntrance(){
        this.group.translate(0, -500);
        let randelay = Math.random()*500
        anime({
            targets: this.group.position,
            x: (1256) * window.innerWidth / window.standardresolution.width, 
            y: (115) * window.innerWidth / window.standardresolution.width, 
            easing: 'easeOutElastic(1, 2)',
            delay: randelay,
        });
    }

    getCreateArcInfo(degrees,center,radius){
        return {
            from: {
                x:center.x + radius,
                y: center.y
            },
            through: {
                x: center.x + Math.cos(degrees/2) * radius,
                y: center.y + Math.sin(degrees/2) * radius
            },
            to: {
                x: center.x + Math.cos(degrees) * radius,
                y: center.y + Math.sin(degrees) * radius
            },
            fillColor: 'black',
            opacity: 0.35,
        }
    }   
    
    updatetimearc(number){
        this.group.children['timearc'].remove();
        let scale = window.innerWidth / window.standardresolution.width
        let arcValues = this.getCreateArcInfo( 
            number, 
            { 
                x: this.timecirclebg.position.getX(), 
                y: this.timecirclebg.position.getY() - 10 * scale 
            }, 
            this.timecirclebg.bounds.width/2
        )
        this.timearc = new paper.Path.Arc(arcValues)
        this.timearc.add(new paper.Point(this.timecirclecenter.position))
        this.timearc.name = 'timearc';
        this.timearc.rotate(-86, this.timecirclecenter.position)
        this.timearc.scale(0.9, 0.8)
        this.group.addChild(this.timearc);
        this.resetresponsivescaling()
    }

    correctcardplaced(){
        // clear and update the animation of the arc
        clearInterval(this.timeticking)
        this.ticker = 6;
        this.timeticking = setInterval(function(){
            this.ticker-=0.03;
            this.updatetimearc(this.ticker);
            if (this.ticker <= 0){
                this.ticker = 0; 
                clearInterval(this.timeticking)
                this.updatetimearc(0)
                this.multiplicative = 1;
                this.titlescoremultiply.content = ' ';
            }
        }.bind(this), 30);

        // update score number
        this.score+= (this.multiplicative * 1); 
        this.titlescorenumber.content = this.score;
        
        // update multiplicative label and value
        if (this.multiplicative < 5)
            this.multiplicative += 1;
        this.titlescoremultiply.content = 'x' + this.multiplicative;
    }
  }
  